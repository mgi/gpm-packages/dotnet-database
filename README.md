This is a _MGI Database Interface_ implementation of the .NET System.Data.DbConnection class.

This allows LabVIEW to communicate with any database that provides a .NET driver.

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2017_
